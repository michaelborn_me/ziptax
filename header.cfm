<html>
<head>
    <title><cfoutput>#application.name#</cfoutput></title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" rel="stylesheet" />
</head>
<body>
    <div class="hero is-dark">
        <div class="hero-body">
        <div class="container">
            <h1 class="title"><cfoutput>#application.name#</cfoutput></h1>
        </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="columns">
            <div class="column is-one-quarter">
                <nav class="menu" role="navigation" aria-label="main navigation">
                <ul class="menu-list">
                    <li><a class="navbar-item" href="/index.cfm">README</a></li>
                    <li><a class="navbar-item" href="/demos/getbyaddress.cfm">Tax Rates by Address</a></li>
                    <li><a class="navbar-item" href="/demos/getbyzip.cfm">Tax Rates by Zip</a></li>
                </ul>
                </nav>
            </div><!---onethird--->
            <div class="column is-three-quarters">