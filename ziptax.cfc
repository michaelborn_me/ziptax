/**
 * ZipTax API Integration
 */
component hint="ZipTax API Integration" displayname="ziptax" {
	/* 
		API Reference: http://docs.zip-tax.com
	*/

	//  This is really not meant to be accessed outside of this CFC! 
	variables._apiToken = "DONT_USE";
	variables._apiURL = "https://api.zip-tax.com";
	/* 
        Please note, this is not the version number for this CFC,
        but the version number for the ZipTax API.
        This version number string is used in the request URL,
        like http://api.zip-tax.com/request/v20
    */
	variables._apiVersion = "v20";

	variables._CODES = {
        INVALIDKEY = "101",
        INVALIDSTATE = "102",
        INVALIDCITY = "103",
        INVALIDPOSTALCODE = "104",
        INVALIDFORMAT = "105"
    };

	public component function init(required string apiKey) output=false {
		variables._apiToken = arguments.apiKey;
		return this;
	}

	private function _parseRequest(required httpReq) output=false {
		if ( arguments.httpReq.responseheader.status_code > "299" ) {
			var retval = {"error":true,"message":"API returned status #arguments.httpReq.responseheader.status_code# #arguments.httpReq.responseheader.explanation#"};
		} else if ( arguments.httpReq.filecontent == "" ) {
			var retval = {"error":true,"message":"No response from API"};
		} else {
			var retval = httpReq.filecontent;
		}
		return retval;
	}

	private function _sendRequest(endpoint="", urlData="", formData="", method="GET", body="") output=false {
		//  url is like this: http://api.zip-tax.com/request/v20 
		var requestURL = "#variables._apiURL#/#arguments.endpoint#/#variables._apiVersion#";
		cfhttp( url=requestURL, result="response", method=arguments.method ) {
			if ( arguments.body != "" ) {
				cfhttpparam( type="body", value=arguments.body );
			}
			//  Note that ZipTax uses a URL parameter for the API key 
			cfhttpparam( name="key", type="url", value=variables._apiToken );
			for ( curArg in arguments.urlData ) {
				cfhttpparam( name=LCase(curArg), type="url", value=arguments['urlData'][curArg] );
			}
			for ( curArg in arguments.formData ) {
				cfhttpparam( name=LCase(curArg), type="formfield", value=arguments['formData'][curArg] );
			}
		}
		return this._parseRequest(response);
	}

	/**
	 * Normalize the tax rate data to match a certain standard.
	 * @description Returns a struct of data, even if it's just an error property and a bunch of zeros and empty strings for the tax/geo info.
	 */
	private struct function _normalize(required results) output=false {
		var taxdata = {
            zip = "",
            city = "",
            county = "",
            state = "",
            cityTax = 0,
            countyTax = 0,
            stateTax = 0,
            districtTax = 0,
            combinedTax = 0
        };
		if ( isJson(arguments.results) ) {
			arguments.results = DeSerializeJSON(arguments.results);
		}
		switch ( arguments.results.rCode ) {
			case  variables._CODES.INVALIDKEY:
				taxdata.error = "Invalid API key";
				break;
			case  variables._CODES.INVALIDSTATE:
				taxdata.error = "Invalid state";
				break;
			case  variables._CODES.INVALIDCITY:
				taxdata.error = "Invalid city";
				break;
			case  variables._CODES.INVALIDPOSTALCODE:
				taxdata.error = "Invalid zip code";
				break;
			case  variables._CODES.INVALIDFORMAT:
				taxdata.error = "Incorrectly formatted address.";
				break;
			default:
				//  do NOT set error string 
				break;
		}
		/* 
        <cfif cgi.remote_addr EQ "216.171.186.170">
            <cfdump var="#arguments.results#" />
        </cfif>*/
		/*  please note that when given just a zip code,
            zip-tax will return multiple "cities" with tax rates for each.
            We really only care about an "estimate", so we just grab the rate from the first one
            and let the user fill out their address to get a better number later.
        */
		if ( isArray(arguments.results.results) && arrayLen(arguments.results.results) > 0 ) {
			var taxes = arguments.results.results[1];
			taxdata.stateTax = taxes.stateSalesTax;
			taxdata.cityTax = taxes.citySalesTax;
			taxdata.countyTax = taxes.countySalesTax;
			taxdata.combinedTax = taxes.taxSales;
			taxdata.districtTax = taxes.districtSalesTax;
		} else {
			if ( !StructKeyExists(taxdata,"error") ) {
				taxdata.error = "No sales tax response: Bad Address?";
			}
		}
		return taxdata;
	}

	/**
	 * Get tax rates by zip or address.
	 */
	public function getTaxRatesByLocation(required zip, city, street, country) output=false {
		var params = {};
		if ( StructKeyExists(arguments,"zip") ) {
			params.postalcode = arguments.zip;
		}
		if ( StructKeyExists(arguments,"city") ) {
			params.city = arguments.city;
		}
		if ( StructKeyExists(arguments,"street") ) {
			params.street = arguments.street;
		}
		if ( StructKeyExists(arguments,"country") ) {
			params.country = arguments.country;
		}
		params.format = "JSON";

		result = this._sendRequest(
			endpoint = "/request/",
			urlData = params
		);
		
        return this._normalize(result);
	}
}