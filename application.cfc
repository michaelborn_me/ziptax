component {
  this.name = "ZipTaxAPIIntegration";
  
  public void function onApplicationStart() {
    loadConfig();
  }
  
  package struct function loadConfig() {
    // load config vars into application scope
    include "/config.cfm";
    structAppend( application, variables.config, false );

    return variables.config;
  }
	public void function onRequestStart() {
		// useful for development - reload the application and config variables.
		if ( StructKeyExists(url,"appreload") ) {
			applicationStop();
      location( cgi.script_name, false );
		}
	}

  public void function onRequest(targetPage) {
      include "/header.cfm";
      include arguments.targetPage;
      include "/footer.cfm";
  }
	
	public void function onError(struct e, string eventName = "[ERROR]") {
		writeOutput("<h2>#arguments.e.message#</h2>");
		writeDump(e);
	}
}
