<h2 class="title is-2">Get Tax Rates by Full Address</h2>

<pre>
ZipTax = New ziptax( application.apiKey );
ratesRequest = ZipTax.getTaxRatesByLocation(
    street = "Pennsylvania Ave",
    city = "Washington",
    country = "US",
    zip = "20500"
);
writeDump(ratesRequest);
</pre>

<cfscript>
ZipTax = New ziptax( application.apiKey );
ratesRequest = ZipTax.getTaxRatesByLocation(
    street = "Pennsylvania Ave",
    city = "Washington",
    country = "US",
    zip = "20500"
);
writeDump(ratesRequest);
</cfscript>
