# ZipTax API Integration

Use [ZipTax][1] for retrieving tax rates by zip, address, etc.

## Getting Started

1. Install module via CommandBox. `install ziptaxcfc`
2. Grab API key from your [ZipTax account][2].
3. Load the `ziptax.cfc` with the API key.

## Docs

### Load ZipTax

Here's where you pass in the api key / version / url, and where your code will differ from the demos.

```js
ZipTax = New ziptax.ziptax( application.apiKey );
```

### Get Tax Rates

Right now there's only a single method to retrieve tax rates: `getTaxRatesByLocation()`.

```js
ZipTax.getTaxRatesByLocation( ... );
```

#### Get By Zip

```js
ZipTax.getTaxRatesByLocation( myZipCode );
```

#### Get By Address

Please note that `zip` is still required.

```js
ZipTax.getTaxRatesByLocation(
    street = "Pennsylvania Ave",
    city = "Washington",
    country = "US",
    zip = "20500"
);
```

## TODO

* Mockbox and testbox for tests
* DocBox for documenting the `ziptax` cfc
* CFLint cuz it's useful

[1]: http://zip-tax.com/
[2]: https://account.zip-tax.com/
